//Patrycja Lewandowska s176702
#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <cstdlib>
#include <ctime>
using namespace std;

vector<bitset<8>> Bit_parzystoscii(vector<bitset<8>> bvc, vector<char> znaki ){

    int ones = 0;

    cout<<"Bit parzystosci"<<endl;
    ofstream plik2("plik_bit_parzystosci.txt");

    plik2<<"Bit parzystosci"<<endl;

    int l = znaki.size();

    for(int i =0 ; i < l ; i++){

        for(int j = 0; j < 8 ; j++){
            if(bvc[i][j]==1){
                ones++;
            }
        }

        if(ones%2 == 0){
            plik2 << bvc[i] <<'0'<<endl;
            bvc.push_back(0);
        }
        else if(ones%2 != 0) {
            plik2 << bvc[i] <<'1'<<endl;
            // w pliku dane i bit parzystosci
            bvc.push_back(1);
        }
        else {
            cout<<"Error - bit parzystosci - zliczanie jedynek"<<endl;
        }

        ones = 0;
    }

   /* for(int k =0 ; k<znaki.size(); k++){
        plik2<<znaki[k];
            if()

    }*/

    plik2.close();

    return bvc;

}

void suma_modulo(vector<bool> msg){

// 1 + 1 = 10
// 1 + 0 = 1
// 0 + 1 = 1
// 0 + 0 = 0
// 10 + 1 = 11
vector<bool> wynik ={0,0,0,0,0,0,0,0};
vector<bool> msg8;
vector<bool> kopia;
vector<bool> nowy_wynik;

int c = 0;
int bec = msg.size();

for(int as = 0; as < bec/8 ; as ++){

        for(int s = 0; s<8; s++){
            bool index = msg[as*8 + s];
            msg8.push_back(index);
        }


if( wynik.size() != msg8.size()){
          int roznica = wynik.size() - msg8.size();
          for(int it = 0 ; it<roznica ; it++){

          msg8.insert(msg8.begin(),false);}

        }

for(int x = wynik.size()-1; x >= 0 ; x--){
    if( c == 0 ){

        if(wynik[x] == msg8[x]){
                if(wynik[x] == 0 ){
                    nowy_wynik.push_back(false);
                    c = 0;
                    continue;
                }
                if (wynik[x] == 1){
                    nowy_wynik.push_back(false);
                    c = 1;
                    continue;
                }
        }
        if(wynik[x] != msg8[x]){
            nowy_wynik.push_back(true);
            c = 0;
            continue;
        }
    }

    if( c == 1){

        if(wynik[x] == msg8[x]){
            if(wynik[x] == 0){
                nowy_wynik.push_back(true);
                c = 0;
                continue;
            }

            if(wynik[x] == 1){
                nowy_wynik.push_back(true);
                c = 1;
                continue;
            }
        }
        if( wynik[x] != msg8[x]){
            nowy_wynik.push_back(false);
            c = 1;
            continue;
        }
    }
}
    if ( c == 1){
        nowy_wynik.push_back(true);
    }

    for(int k = nowy_wynik.size()-1 ; k >= 0; k --){
        kopia.push_back(nowy_wynik[k]);
    }

    wynik = kopia;
    nowy_wynik.clear();
    kopia.clear();
    msg8.clear();
    c = 0;
}
    cout<<"Suma modulo"<<endl;
    ofstream plik2("plik_suma_modulo.txt");
    plik2<<"wynik: "<<endl;
    cout<<"wynik: "<<endl;
    for(int k = 0 ; k<wynik.size(); k ++){
        plik2<<wynik[k];
        cout<<wynik[k];
    }

    plik2.close();
}

//crc8 - wielomian  x^8 + x^2 + x + 1, czyli jest to  100000111 binarnie

void CRC(vector<bool> msg,vector<bool> org){

vector<bool> dzielnik;
/*
if(wcrc == 8){
    dzielnik = {1,0,0,0,0,0,1,1,1};
}
if(wcrc == 16){
    dzielnik = {1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1};
}
if(wcrc == 32){
    dzielnik = {1,0,0,0,0,0,1,0,0,1,1,0,0,0,0,0,1,0,0,0,1,1,1,0,1,1,0,1,1,0,1,1,1};
}*/
bool wynik_xor;
dzielnik = {1,0,0,0,0,0,1,1,1};


for(int h = 0; h<8; h++){
    msg.push_back(false);
}



for(int sa = 0; sa<msg.size(); sa++){


        if(msg[sa] == 1){
            if(sa > 7){break;}

            for(int j = 0; j < dzielnik.size(); j++){
                wynik_xor =  (msg[sa]+dzielnik[j])%2;
                msg[sa] = wynik_xor;
                sa++;

                if(j==dzielnik.size()-1){
                sa=0;
                }
            }

        }
}
ofstream plik2("plik_CRC.txt");
for(int h = 0; h<msg.size(); h++){
    cout<<msg[h];
    plik2<<msg[h];
    }

    cout<<endl;
    plik2<<endl;



}



/*void Bit_parzystosci(vector<bool> msg ){

int ones_ = 0;

    for(int h = 0 ; h<msg.size(); h ++){
         if( msg[h] == true)
        {
            ones_++;
        }
    }

   if(ones_%2 == 0)
   {
       msg.push_back(false);
   }
   else{
        msg.push_back(true);
   }

   for(int h = 0 ; h<msg.size(); h ++){
         cout<<msg[h];
    }
}*/


int main()
{

    ifstream plik;

    plik.open("plik.txt",  ios::in | ios::out | ios::binary  );
    if(!plik.is_open()) {cout<<"Blad podczas otwierania pliku";}


    cout<<"Wybierz algorytm, z ktorego chcesz skorzystac:\n"
            <<"1 - Bit parzystosci\n"
            <<"2 - Suma modulo\n"
            <<"3 - CRC\n";


    int wybor;
    cin>>wybor;

    vector<char> znaki;
    string line;
    vector<bitset<8>> bvc;

       /* vector<char> buffer((
            std::istreambuf_iterator<char>(plik)),
            (std::istreambuf_iterator<char>())); */



  /* test pliku.txt  */
    while(getline(plik, line)){
            for(int i = 0; i < line.size(); i++){
                znaki.push_back(line[i]);
                bvc.push_back(bitset<8>(znaki[i]));}}

plik.close();
       /* for(int i = 0; i < buffer.size(); i++){

                bvc.push_back(bitset<8>(buffer[i]));} */


////////////////////////////////////////////////////
   vector<bool> msg ;
    for(int b = 0 ; b<bvc.size()  ; b++){


        for(int k = 7; k>=0; k-- ){

            if(bvc[b][k] == 1){
                msg.push_back(true);

            }
             else if( bvc[b][k] == 0){
                msg.push_back(false);
            }
            else {cout<<"blad przy zamianie"<<endl;}
        }

    }

  vector<bool> a;
/*if(wybor == 1){

        for(int i = 0 ; i <msg.size() ; i++){
            for(int it=0; it<8;it++){
                a.push_back(msg[it]);
                if(it != 7){
                    i++;
                }
        Bit_parzystosci(a);
        cout<<endl;
        a.clear();
            }
        }
  }
  */
  if(wybor == 1)
  {
      bvc = Bit_parzystoscii(bvc,znaki);
  }

    if( wybor == 2){
       suma_modulo(msg);
    }

int wybor_crc;

    if( wybor == 3){

    //cout<<"wybierz wersje CRC"<<endl;
    //  cin>>wybor_crc;
     //   if(wybor_crc == 8 || wybor_crc == 16 || wybor_crc == 32 ){


        for(int g = 0 ; g <msg.size(); g++){
            for(int it=0; it<8;it++){
                a.push_back(msg[g]);
                if(it != 7){
                    g++;
                }
            }
            CRC(a,a );
            a.clear();
        }
    }
int dlugosc = bvc.size();


 ////////////////////////////////////////////////////////////////////////////////
 //STRUKTURA TESTOWA

    struct paczka{
        // nag�owek
        int id;;

        int rozmiar;

        double nr_pakietu;

        bitset<8> dane;

        bitset<8> suma_kontrolna;

    };

    paczka tablica[bvc.size()/2];





    for(int d=0; d<dlugosc/2; d++){
        tablica[d].id = 1;
        tablica[d].nr_pakietu = d;
        tablica[d].rozmiar = bvc[d].size();
        tablica[d].dane = bvc[d];
        tablica[d].suma_kontrolna = bvc[dlugosc/2 + d];
    }
      for(int d=0; d<dlugosc/2; d++){
        cout<<tablica[d].id<<" ";
        cout<<tablica[d].nr_pakietu<<" ";
        cout<<tablica[d].rozmiar<<" ";
        cout<<tablica[d].dane<<" ";
        cout<<tablica[d].suma_kontrolna<<endl;
    }


    srand(time(NULL));
    for(int a = 0; a<dlugosc/2; a++){
    int x = 1;//and()%10;
    if(x == 1){
        int liczba = rand()% bvc.size();
        int liczbav2 = rand()%7;

            if(bvc[liczba][liczbav2] == 0){
                bvc[liczba][liczbav2] = 1;
            }

            else {
                 bvc[liczba][liczbav2] = 0;
            }
    }}

cout<<endl;

    for(int d=0; d<dlugosc/2; d++){
        tablica[d].id = 1;
        tablica[d].nr_pakietu = d;
        tablica[d].rozmiar = bvc[d].size();
        tablica[d].dane = bvc[d];
        tablica[d].suma_kontrolna = bvc[dlugosc/2 + d];}


      for(int d=0; d<dlugosc/2; d++){
        cout<<tablica[d].id<<" ";
        cout<<tablica[d].nr_pakietu<<" ";
        cout<<tablica[d].rozmiar<<" ";
        cout<<tablica[d].dane<<" ";
        cout<<tablica[d].suma_kontrolna<<endl;
    }


}
