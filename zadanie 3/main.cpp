#include<cstdlib>
#include<iomanip>
#include <stack>
#include <iostream>
#include<algorithm>
#include<vector>

using namespace std;

struct Krawedz{

    int wierzch1;
    int wierzch2;
    int waga;
    bool dodana;
    bool rozpatrzona;
    bool vec;
    Krawedz(void);
};

Krawedz::Krawedz(void)
{
   dodana = false;
   vec = false;
   rozpatrzona = false;
}

void bombelkowe(Krawedz *tab_krawedz, int n)
{
    Krawedz x;
    for (int i=0; i<n; i++)
    {
        for( int j=n-1; j>=1; j--)
        {
            if(tab_krawedz[j].waga<tab_krawedz[j-1].waga)
            {

                swap(tab_krawedz[j-1],tab_krawedz[j]);

            }
        }
    }

}


int main()
{
    int wierzcholki;
    int krawedzie;
    int suma_drzewa = 0;

   cout<<"Ilosc wierzcholkow: ";
   cin>>wierzcholki;

   cout<<"Ilosc krawedzi: ";
   cin>>krawedzie;

   if(krawedzie == 0 || wierzcholki == 0) {cout<<"Brak danych"; return 1;}
   if(krawedzie+1 != wierzcholki && krawedzie<wierzcholki) {cout<<"Zbyt malo krawedzi dla tylu wiercholkow"; return 2;}

   int czy_krawedz_istnieje[wierzcholki][wierzcholki];
   int czy_krawedz_jest_dodana[wierzcholki][wierzcholki];

   for(int j = 0; j< wierzcholki; j++){
        for(int k = 0; k< wierzcholki; k++){
          czy_krawedz_jest_dodana[j][k] = 0;
        czy_krawedz_istnieje[j][k] = 0;

        }
    }

    vector<int> odwiedzone_wierzcholki;

   Krawedz tab_krawedz[krawedzie];

   for(int i = 0 ; i<krawedzie ; i++){
    cout<<"Krawedz: "<<i+1<<endl;
    cout<<"Wierzcholek 1: ";
    cin>>tab_krawedz[i].wierzch1;

    cout<<"Wierzcholek 2: ";
    cin>>tab_krawedz[i].wierzch2;

    cout<<"Waga krawedzi: ";
    cin>>tab_krawedz[i].waga;

    czy_krawedz_istnieje[tab_krawedz[i].wierzch1][tab_krawedz[i].wierzch2] = 1;
    czy_krawedz_istnieje[tab_krawedz[i].wierzch2][tab_krawedz[i].wierzch1] = 1;


   }

    bombelkowe(tab_krawedz, krawedzie);

    int rodzic;int sasiad;
    vector<int> vector_badanych_krawedzi;
    vector<int> vector_rozpatrzonych_krawedzi;
    int g = 0;
    int wierzch = 1;
    bool cykl;
    bool powtorka;

    while(wierzch != wierzcholki){

    rodzic = tab_krawedz[g].wierzch1;

    sasiad = tab_krawedz[g].wierzch2;

    vector_badanych_krawedzi.push_back(rodzic);


    while(cykl == false){


    for(int h = 0; h<wierzcholki; h++ ){

        powtorka = false;

        if(czy_krawedz_istnieje[rodzic][h] == 0 || czy_krawedz_istnieje[h][rodzic] == 0){continue;}


        if(czy_krawedz_jest_dodana[rodzic][h] == 0 || czy_krawedz_jest_dodana[h][rodzic] == 0){continue;}


         for(int j = 0; j<vector_badanych_krawedzi.size();j++){
            if(vector_badanych_krawedzi[j] == h ){powtorka = true; }

        }
        for(int j = 0; j<vector_rozpatrzonych_krawedzi.size();j++){
            if(vector_rozpatrzonych_krawedzi[j] == h){powtorka = true;}

        }


        if( powtorka == true ){continue;}

        vector_badanych_krawedzi.push_back(h);


        rodzic = h;
        h = 0;
       }
    vector_badanych_krawedzi.pop_back();
    vector_rozpatrzonych_krawedzi.push_back(rodzic);


    for(int jj = 0; jj<vector_rozpatrzonych_krawedzi.size();jj++){

            if(vector_rozpatrzonych_krawedzi[jj] == rodzic){
                    for(int ji = 0; ji<vector_rozpatrzonych_krawedzi.size();ji++){
                        if(vector_rozpatrzonych_krawedzi[ji] == sasiad){
                          cykl = true;break;
                        }
                    }

                    if(cykl == true){break;}}}

    rodzic = vector_badanych_krawedzi[vector_badanych_krawedzi.size()];
    if(vector_badanych_krawedzi.empty()){break;}


    }

    g++;
    vector_rozpatrzonych_krawedzi.clear();
     if(cykl == true){ cykl = false; continue;}
    tab_krawedz[g-1].dodana = true;
    czy_krawedz_jest_dodana[tab_krawedz[g-1].wierzch1][tab_krawedz[g-1].wierzch2] = 1;
    czy_krawedz_jest_dodana[tab_krawedz[g-1].wierzch2][tab_krawedz[g-1].wierzch1] = 1;
    wierzch++;

    }

     cout<<"Mimalne drzewo rozpinajace - algorytm Kruskala"<<endl;

      for(int k =0; k<krawedzie; k++){
        if(tab_krawedz[k].dodana == true){
        cout<<tab_krawedz[k].wierzch1<<" -> "<<tab_krawedz[k].wierzch2<<" waga: "<<tab_krawedz[k].waga<<endl;
        suma_drzewa=suma_drzewa+tab_krawedz[k].waga;
        }

    }
    cout<<"Suma minimalnego drzewa rozpinajacego: "<<suma_drzewa;

     for(int sa = 0 ; sa<krawedzie ; sa++){

        tab_krawedz[sa].dodana = false;
    }


     int index = 0;

    rodzic = tab_krawedz[0].wierzch1;
    int inna_waga;
    int zapis = true;
    vector<int> odwiedzone_wierz;
    odwiedzone_wierz.push_back(rodzic);
    int a = 0;
    while(odwiedzone_wierz.size() != wierzcholki){

        for(int i = 0 ; i < krawedzie; i++){
             if( (rodzic == tab_krawedz[i].wierzch1 || rodzic == tab_krawedz[i].wierzch2)  && tab_krawedz[i].dodana == false && tab_krawedz[i].rozpatrzona == false ){
                    tab_krawedz[i].vec = true;
            }
        }



         for(int i = 0 ; i < krawedzie; i++){
             if( tab_krawedz[i].vec == true && tab_krawedz[i].dodana == false && tab_krawedz[i].rozpatrzona == false  ){
                        if(zapis == true){inna_waga = tab_krawedz[i].waga; zapis = false;}

                if( inna_waga >= tab_krawedz[i].waga){
                    inna_waga = tab_krawedz[i].waga;
                    a = i;

                }

            }
        }

        for(int i =0; i <odwiedzone_wierz.size(); i++){

          if(odwiedzone_wierz[i] == tab_krawedz[a].wierzch2 )  {

            for(int s = 0 ; s < odwiedzone_wierz.size(); s ++){

            if(odwiedzone_wierz[s] == tab_krawedz[a].wierzch1 ) {tab_krawedz[a].rozpatrzona = true;  break;}


    }}}

    index++;
    zapis = true;

    if( tab_krawedz[a].rozpatrzona == false && tab_krawedz[a].dodana == false ){

    tab_krawedz[a].dodana = true;

    tab_krawedz[a].rozpatrzona = true;


 for(int i =0; i <odwiedzone_wierz.size(); i++){


       if(odwiedzone_wierz[i] == tab_krawedz[a].wierzch2 ){ odwiedzone_wierz.push_back(tab_krawedz[a].wierzch1); rodzic = tab_krawedz[a].wierzch1; break;}
       if(odwiedzone_wierz[i] == tab_krawedz[a].wierzch1 ) {odwiedzone_wierz.push_back(tab_krawedz[a].wierzch2); rodzic = tab_krawedz[a].wierzch2; break;}

    }

}
}


    cout<<endl<<"Mimalne drzewo rozpinajace - algorytm Prima"<<endl;
        suma_drzewa = 0;
      for(int i =0; i <krawedzie; i++){

            if(tab_krawedz[i].dodana == true && tab_krawedz[i].rozpatrzona == true) {
             suma_drzewa=suma_drzewa+tab_krawedz[i].waga;

            cout<<tab_krawedz[i].wierzch1<<" -> "<<tab_krawedz[i].wierzch2<<" waga :"<<tab_krawedz[i].waga<<endl;}
        }


        cout<<" suma drzewa : "<<suma_drzewa;





    return 0;
}
